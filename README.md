This package contains several common hash functions.

Hash algorithms are implemented in a generic way. This means that new hash algorithms can easily be implemented then distributed to existing code with minimal changes.

The following algorithms are implemented in native LabVIEW code:

- MD5
- SHA1
- SHA224
- SHA256
- SHA384
- SHA512
- HMAC using any hash algorithm as a base

### Note

Since these functions are implemented in native LabVIEW they should not be used in an application where security is vital. There are vectors of attack that will let malicious actors affect the results, which could compromise security (think memory editing).

These VIs are useful for tasks like generating signatures when communicating to a remote API, or when the security of the environment can be verified.

#### Use at your own risk!

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
